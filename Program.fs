﻿namespace Nenen
open System

open Microsoft.AspNetCore.Builder
open Microsoft.AspNetCore.Hosting
open Nancy
open Nancy.Owin

type NenenBootstrapper() =
  inherit DefaultNancyBootstrapper()

type Startup() =
  member this.Configure(app : IApplicationBuilder) =
    app.UseOwin(fun x -> x.UseNancy(fun u -> u.Bootstrapper <- new NenenBootstrapper()) |> ignore) |> ignore


module App =
  [<EntryPoint>]
  let main argv = 
    use host = (new WebHostBuilder()).UseKestrel().UseStartup<Startup>().Build()
    host.Run()
    0 // return an integer exit code
