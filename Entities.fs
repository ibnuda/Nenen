namespace Nenen

module Entities =
  open Chiron

  type Ids =
    | CategoryId of string
    | CommentId of string
    | PageId of string
    | PostId of string
    | UserId of string
    | WebLogId of string
    static member ToJson (ids : Ids) =
      let idkey, idvalue =
        match ids with
        | CategoryId cid -> "categoryid", cid
        | CommentId cid -> "commentid", cid
        | PageId pid -> "pageid", pid
        | PostId pid -> "postid", pid
        | UserId uid -> "userid", uid
        | WebLogId wlid -> "weblogid", wlid
      json {
        do! Json.write idkey idvalue
      }
  (*  static member FromJson (_ : Ids) =
      fun *)

  type CategoryId = CategoryId of string
  type CommentId = CommentId of string
  type PageId = PageId of string
  type PostId = PostId of string
  type UserId = UserId of string
  type WebLogId = WebLogId of string

  type Permalink = Permalink of string
  type Tag = Tag of string
  type Ticks = Ticks of int64
  type TimeZone = TimeZone of string
  type Url = Url of string

  type AuthLevel =
    | Admin
    | User

  type UserUser =
    { Name : string }

  type PostStatus =
    | Draft
    | Published

  type CommentStatus =
    | Approved
    | Pending
    | Spam

  type Revision =
    { AsOf : int64
      Text : string }
    static member Empty =
      { AsOf = 0L
        Text = "" }
    static member ToJson (revision : Revision) =
      json {
        do! Json.write "asof" revision.AsOf
        do! Json.write "text" revision.Text
      }
    static member FromJson (_ : Revision) =
      json {
        let! asof = Json.read "asof"
        let! text = Json.read "text"
        return
          { AsOf = asof
            Text = text }
      }

  type Page =
    { Id : PageId
      WebLogId : WebLogId
      AuthorId : UserId
      Title : string
      Permalink : Permalink
      PublishedOn : Ticks
      UpdatedOn : Ticks
      ShowInPageList : bool
      Text : string
      Revision : Revision list }
    static member Empty =
      { Id = PageId ""
        WebLogId = WebLogId ""
        AuthorId = UserId ""
        Title = ""
        Permalink = Permalink ""
        PublishedOn = Ticks 0L
        UpdatedOn = Ticks 0L
        ShowInPageList = false
        Text = ""
        Revision = [] }
  (*  static member ToJson (page : Page) =
      json {
        do! Json.write "id" page.Id
        do! Json.write "weblogid" page.WebLogId
        do! Json.write "authorid" page.AuthorId
        do! Json.write "title" page.Title
        do! Json.write "permalink" page.Permalink
        do! Json.write "publishedon" page.PublishedOn
        do! Json.write "updatedon" page.UpdatedOn
        do! Json.write "showinpagelist" page.ShowInPageList
        do! Json.write "text" page.Text
        do! Json.write "revision" page.Revision
      }*)

  type WebLog =
    { Id : WebLogId
      Name : string
      Subtitle : string option
      DefaultPage : string
      ThemePath : string
      UrlBase : string
      TimeZone : TimeZone }
    static member Empty =
      { Id = WebLogId ""
        Name = ""
        Subtitle = None
        DefaultPage = ""
        ThemePath = "default"
        UrlBase = ""
        TimeZone = TimeZone "Asia/Jakarta" }

  type Authorization =
    { WebLogId : WebLogId
      Level : AuthLevel }
    
  type User =
    { Id : UserId
      EmailAddress : string 
      PasswordHash : string
      Name : string
      Url : Url option
      Authorizations : Authorization list }
    static member Empty =
      { Id = UserId ""
        EmailAddress = ""
        PasswordHash = ""
        Name = ""
        Url = None
        Authorizations = [] }

  type Category =
    { Id : CategoryId
      WebLogId : WebLogId
      Name : string
      Slug : string
      Description : string option
      ParentId : CategoryId option
      Children : CategoryId list }
    static member Empty =
      { Id = CategoryId "new"
        WebLogId = WebLogId ""
        Name = ""
        Slug = ""
        Description = None
        ParentId = None
        Children = [] }

  type Comment =
    { Id : CommentId
      PostId : PostId
      InReplyToId : CommentId option
      Name : string
      EmailAddress : string
      Url : Url option
      Status : CommentStatus
      PostedOn : Ticks
      Text : string }
    static member Empty =
      { Id = CommentId ""
        PostId = PostId ""
        InReplyToId = None
        Name = ""
        EmailAddress = ""
        Url = None
        Status = Pending
        PostedOn = Ticks 0L
        Text = "" }

  type Post =
    { Id : PostId
      WebLogId : WebLogId
      AuthorId : UserId
      Status : PostStatus
      Title : string
      Permalink : Permalink
      PublishedOn : Ticks
      UpdatedOn : Ticks
      Text : string
      CategoryIds : CategoryId list
      Tags : Tag list
      Revisions : Revision list }
    static member Empty =
      { Id = PostId ""
        WebLogId = WebLogId ""
        AuthorId = UserId ""
        Status = Draft
        Title = ""
        Permalink = Permalink ""
        PublishedOn = Ticks 0L
        UpdatedOn = Ticks 0L
        Text = ""
        CategoryIds = []
        Tags = []
        Revisions = [] }