[<AutoOpen>]
module Extension

open System.Threading.Tasks

type AsyncBuilder with
  member x.Bind(t : Task<'T>, f : 'T -> Async<'U>) : Async<'U> =
    async.Bind (Async.AwaitTask t, f)
  
  member x.Bind(t : Task, f : unit -> Async<'U>) : Async<'U> =
    async.Bind (Async.AwaitTask t, f)

  member x.ReturnFrom(t : Task<'T>) : Async<'T> =
    Async.AwaitTask t