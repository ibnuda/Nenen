namespace Nenen

module HomeModule =
  open Nancy

  type HomeModule() as this =
    inherit NancyModule()

    do
      this.Get("/", fun _ -> "Wat")
      this.Get("/{category}", fun something -> something ? category)
      this.Get("/{category}/{subcategory}",
        fun something -> (something ? category).ToString() + " with sub category : " + (something ? subcategory).ToString() )

      this.Post("/", fun _ -> HttpStatusCode.FailedDependency)