namespace Nenen

module AboutModule =
  open Nancy

  type AboutModule() as this =
    inherit NancyModule()

    do
      this.Get("/about", fun _ -> "About")
