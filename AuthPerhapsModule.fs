namespace Nenen

module AuthPerhapsModule =
  open Nancy
  open Nenen.AboutModule

  type Kuda =
    { Nama : string
      Jenis : string }
  type AuthPerhapsModule() as this =
    inherit NancyModule()

    do
      this.Get("/auth",
        fun param ->
          let identity = this.Context.CurrentUser
          // let name = Entities.UserUser { Name = identity.Identity.Name }
          this.Response.AsJson(
            Nenen.Entities.Page.Empty
          )
      )

