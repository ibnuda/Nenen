[<AutoOpen>]
module Helper

open Nancy

let (?) (object : obj) (prop : string) : obj =
  (object :?> DynamicDictionary).[prop]